/**
 * C++ program for create my own linux containers
 * Solution is based on:
 *   https://cesarvr.github.io/post/2018-05-22-create-containers/
 *   https://www.thegeekdiary.com/how-to-create-virtual-block-device-loop-device-filesystem-in-linux/
 *   https://www.toptal.com/linux/separation-anxiety-isolating-your-system-with-linux-namespaces
 * 
 * To build: g++ -O3 main.cpp -DDEBUG -o main
 */

#include <iostream>
#include <sched.h>
#include <sys/types.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/wait.h>

#define STACK_SIZE 1048576


int ASSERT(bool status, const char *msg) {
 if(!status) {  
    perror(msg); 
    exit(EXIT_FAILURE);
 }
 return status;
}

void set_host_name(const std::string& hostname) {
  ASSERT(sethostname(hostname.c_str(), hostname.size()) >= 0, "Failed to call sethostname function");
}

void setup_variables() {
  clearenv();  // remove all environment variables for this process
  setenv("TERM", "xterm-256color", 0);
  setenv("PATH", "/bin/:/sbin/:usr/bin:/usr/sbin", 0);
}

void run_system(const char* cmd) {
  int status = system(cmd);
  ASSERT(status == 0, "Failed to run command");
}

std::string run_system(const char* cmd, size_t buffer_size) {
  FILE* pipe = popen(cmd, "r");
  ASSERT(pipe != NULL, "Failed to run command");
  char buffer[buffer_size];
  ASSERT(fgets(buffer, buffer_size, pipe) != NULL, "Failed to call fgets command");
  ASSERT(pclose(pipe) == 0, "Child process failed with non zero code");
  return std::string(buffer);
}

std::string setup_filesystem() {
  bool has_loopback = access("loopbackfile.img", F_OK) == 0;
  if (has_loopback) {
#ifdef DEBUG
    printf("[DEBUG] Found existing loopback file loopbackfile.img\n");
#endif    
  } else {
#ifdef DEBUG
    printf("[DEBUG] Creating loopback file loopbackfile.img (1GB)\n");
#endif
    run_system("dd if=/dev/zero of=loopbackfile.img bs=100M count=10 >/dev/null 2>/dev/null");
  }

  std::string loop_device = run_system("losetup -f --show loopbackfile.img", 64);
  loop_device.erase(loop_device.find_last_not_of(" \t\n\r\f\v") + 1);
#ifdef DEBUG
  printf("[DEBUG] Mounted loop device: %s\n", loop_device.c_str());
#endif

  if (!has_loopback) {
#ifdef DEBUG
    printf("[DEBUG] Creating ext4 filesystem on loopbackfile.img\n");
#endif
    run_system("mkfs.ext4 loopbackfile.img >/dev/null 2>/dev/null");
  }

#ifdef DEBUG
  printf("[DEBUG] Creating temp folder to mount filesystem: ./mnt\n");
#endif
  mkdir("mnt", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

#ifdef DEBUG
  printf("[DEBUG] Mounting loopback device to the temp folder\n");
#endif
  run_system(("mount -o loop " + loop_device + " mnt").c_str());

  if (!has_loopback) {
#ifdef DEBUG
    printf("[DEBUG] Downloading and installing alpine linux\n");
#endif
    run_system("wget -qO- https://nl.alpinelinux.org/alpine/v3.16/releases/x86_64/alpine-minirootfs-3.16.3-x86_64.tar.gz | tar xvz -C mnt > /dev/null");
  }

#ifdef DEBUG
  printf("[DEBUG] The file system has been configured\n");
#endif

  return loop_device;
}

void unsetup_filesystem(const std::string& loop_device) {
#ifdef DEBUG
  printf("[DEBUG] Unmounting loopback device and remove temp folder\n");
#endif
  run_system("umount mnt");
  rmdir("mnt");
#ifdef DEBUG
  printf("[DEBUG] Unmounting loop device: %s\n", loop_device.c_str());
#endif
  run_system(("losetup -d " + loop_device).c_str());
}


int main_container(void* args) {
  char* exec_args[] = {(char *)"/bin/sh", (char *)0};  // enerating the arguments array at compile time
  return execvp("/bin/sh", exec_args);
  return EXIT_SUCCESS;
}

int child_fn(void* args) {
  set_host_name("container");
  setup_variables();

  // Isolate filesystem
  ASSERT(chroot("mnt") == 0, "Failed to chroot");
  ASSERT(chdir("/") == 0, "Failed to chdir");

  mount("proc", "/proc", "proc", 0, 0);

  char stack[STACK_SIZE];
  // Create a new process using system function call with flags:
  //   SIGCHLD - tells the process to emit a signal when finished
  if (clone(main_container, stack + STACK_SIZE, SIGCHLD, NULL) < 0) {
    throw std::runtime_error("Failed to create container. Try to run with root privileges");
  }
  wait(NULL);  // wait for all childs to finish (to prevent them becoming zombies)

  umount("/proc");

  return EXIT_SUCCESS;  
}

int main() {
  printf("Welcome to My linux container!\n");

  std::string loop_device = setup_filesystem();

  char stack[STACK_SIZE];
  // Create a new process using system function call with flags:
  //   SIGCHLD - tells the process to emit a signal when finished
  //   CLONE_NEWUTS - create the process in a new UTS namespace
  //   CLONE_NEWPID - create the process in a new PID namespace
  //   CLONE_NEWNET - create the process in a new NET namespace
  //   CLONE_NEWNS - create the process in a new NS namespace (isolate system’s mountpoint structure)
  if (clone(child_fn, stack + STACK_SIZE, CLONE_NEWPID | CLONE_NEWUTS | CLONE_NEWNET | CLONE_NEWNS | SIGCHLD, NULL) < 0) {
    throw std::runtime_error("Failed to create container. Try to run with root privileges");
  }
  wait(NULL);  // wait for all childs to finish (to prevent them becoming zombies)

  unsetup_filesystem(loop_device);

  return EXIT_SUCCESS;
}
