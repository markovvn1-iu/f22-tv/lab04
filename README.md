<div align="center">
  <img src=".gitlab/logo.svg" height="80px"/><br/>
  <h1>Lab 4</h1>
  <p>Linux Containers</p>
</div>

## 📝 Description

This project contains my own container with isolated PID, MNT and NET written on C++.

## ⚡️ Quick start

```bash
git clone https://gitlab.com/markovvn1-iu/f22-tv/lab04.git && cd lab04
g++ -O3 main.cpp -DDEBUG -o main
sudo ./main  # Tested on Ubuntu 22.04 (x86_64)
```

![](.gitlab/img01.png)

## Tests

As internet forwarding is not part of the task, to install sysbetch you need to stop the container, replace the image with one with sysbetch installed, then start the container.

| Metric                 | Sysbench command                                             | Why this command                  | What is interesting in sysbench output          |
| ---------------------- | ------------------------------------------------------------ | --------------------------------- | ----------------------------------------------- |
| CPU performance        | sysbench --time=60 cpu --cpu-max-prime=64000 run             | Because this command was in lab 3 | Events per second, Latency avg                  |
| CPU thread performance | sysbench --threads=10 --time=60 cpu --cpu-max-prime=64000 run | Because this command was in lab 3 | Events per second, Latency avg                  |
| Scheduler performance  | sysbench --num-threads=64 --test=threads --thread-yields=100 --thread-locks=2 run | Because this command was in lab 3 | Total time, Total number of events, Latency avg |
| Thread memory access   | sysbench --threads=10 --time=60 memory --memory-oper=write run | Because this command was in lab 3 | Total time, Latency max                         |
| Memory access          | sysbench --test=memory --memory-block-size=1M --memory-total-size=10G run | Because this command was in lab 3 | Total time, Latency max                         |
| File I/O read/write    | sysbench --test=fileio --file-total-size=512M --file-test-mode=rndrw  --time=120 --max-time=300 --max-requests=0 run | Because this command was in lab 3 | File operations, Throughput                     |

Result of measurements:

|                                                | Host machine | Multipass VM | My container | Docker     |
| ---------------------------------------------- | ------------ | ------------ | ------------ | ---------- |
| CPU performance (Events per second)            | **122.45**   | 116.78       | 108.03       | 108.52     |
| CPU performance (Latency avg)                  | **8.17**     | 8.56         | 9.26         | 9.21       |
| CPU thread performance (Events per second)     | **614.38**   | 402.021      | 576.97       | 558.35     |
| CPU thread performance (Latency avg)           | **16.27**    | 24.912       | 17.33        | 17.90      |
| Scheduler performance (Total time, s)          | 10.0034      | 10.4118      | 10.0104      | 10.0103    |
| Scheduler performance (Total number of events) | **139325**   | 878          | 66381        | 69006      |
| Scheduler performance (Latency avg)            | **4.59**     | 834.61       | 9.65         | 9.28       |
| Thread memory access (Total time)              | 7.2525       | 9.6059       | 7.0892       | **6.9197** |
| Thread memory access (Latency max)             | 32.52        | 37.37        | 33.64        | **32.04**  |
| Memory access (Total time)                     | **0.3976**   | 0.4754       | 0.6485       | 0.6435     |
| Memory access (Latency max)                    | **0.17**     | 1.23         | 1.70         | 0.99       |
| File I/O (File operations, reads/s)            | 177.42       | **261.21**   | 139.68       | 177.85     |
| File I/O (File operations, writes/s)           | 118.28       | **174.14**   | 93.12        | 118.56     |
| File I/O (File operations, fsyncs/s)           | 378.52       | **557.25**   | 298.22       | 379.74     |
| File I/O (Throughput read, MiB/s)              | 2.77         | **4.08**     | 2.18         | 2.78       |
| File I/O (Throughput written, MiB/s)           | 1.85         | **2.72**     | 1.45         | 1.85       |

The metrics are different because each technology imposes a different overhead on certain operations. The performance of my container is about the same as Docker. I guess we use roughly the same isolation techniques.

## :computer: Contributors

<p>

  :mortar_board: <i>All participants in this project are undergraduate students in the <a href="https://apply.innopolis.university/en/bachelor/">Department of Computer Science</a> <b>@</b> <a href="https://innopolis.university/">Innopolis University</a></i> <br> <br>

  :boy: <b>Vladimir Markov</b> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>Markovvn1@gmail.com</a> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GitLab: <a href="https://gitlab.com/markovvn1">@markovvn1</a> <br>
</p>